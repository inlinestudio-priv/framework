//--------------------------------------------------------
//-- Node IoC - Foundation - Middleware
//--------------------------------------------------------

import __ from '@absolunet/private-registry';


/**
 * Base service provider class.
 *
 * @memberof foundation
 * @abstract
 * @hideconstructor
 */
class Middleware {

	get name() {
		return this.constructor.name;
	}

	handle(req, res, next) {
		return next();
	}

}

export default Middleware;
