"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - HTTP - Repositories - Controller Repository
//--------------------------------------------------------

/**
 * Middleware repository where controller registration and instantiation are done.
 *
 * @memberof http.repositories
 * @hideconstructor
 */
class MiddlewareRepository {
  /**
   * Class dependencies: <code>['app']</code>.
   *
   * @type {Array<string>}
   */
  static get dependencies() {
    return ['app'];
  }
  /**
   * ControllerRepository constructor.
   */


  constructor() {}
  /**
   * Get the controller instance by name.
   * If it was not registered into the repository, it will attempt to find it in the application folder.
   *
   * @param {string} name - The controller name.
   * @returns {http.middleware.Middleware} The controller instance.
   */


  get(name) {
    const fullName = this.getName(name);

    if (this.app.isBound(fullName)) {
      return this.app.make(this.getName(name));
    }

    return this.getFromPath(this.app.middlewarePath(), name);
  }
  /**
   * Check if the given controller was registered.
   *
   * @param {string} name - The controller name.
   * @returns {boolean} Indicates that the controller name was already registered into the repository.
   */


  has(name) {
    return this.app.isBound(this.getName(name));
  }
  /**
   * Make middleware instance from base path and middleware name.
   * Mainly relies on path structure and file name.
   *
   * @param {string} middlewarePath - The middleware file path.
   * @param {string} name - The middleware name.
   * @returns {http.middleware.Middleware} The resolved middleware instance.
   */


  getFromPath(middlewarePath, name) {
    const fileName = this.resolveName(name);
    const filePath = this.app.formatPath(middlewarePath, fileName);
    return this.app.make(filePath);
  }
  /**
   * Get the name that is used for container binding.
   *
   * @param {string} name - The controller name.
   * @returns {string} The controller name into toe container.
   */


  getName(name) {
    const fullName = `middleware.${name}`;
    return this.resolveName(fullName);
  }
  /**
   * Get controller name without container prefix or action.
   *
   * @param {string} name - The controller action.
   * @returns {string} The controller name without the action method.
   */


  resolveName(name) {
    const n = name.replace(this.parametersPattern, '');
    return n.charAt(0).toUpperCase() + n.slice(1);
  }
  /**
   * Get the action from the given qualified controller name.
   *
   * @example
   * this.resolveAction('PostController@show'); // 'show'
   *
   * @param {string} name - The controller action.
   * @returns {string} The controller method name, without the controller name.
   */


  resolveParameters(name) {
    const params = name.match(this.parametersPattern);
    return params ? params.groups.action.split(',') : [];
  }
  /**
   * Action pattern for qualified action.
   *
   * @type {RegExp}
   */


  get parametersPattern() {
    return /:(?<action>.*)$/u;
  }

}

var _default = MiddlewareRepository;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;