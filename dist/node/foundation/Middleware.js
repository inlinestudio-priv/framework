"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _privateRegistry = _interopRequireDefault(require("@absolunet/private-registry"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//--------------------------------------------------------
//-- Node IoC - Foundation - Middleware
//--------------------------------------------------------

/**
 * Base service provider class.
 *
 * @memberof foundation
 * @abstract
 * @hideconstructor
 */
class Middleware {
  get name() {
    return this.constructor.name;
  }

  handle(req, res, next) {
    return next();
  }

}

var _default = Middleware;
exports.default = _default;
module.exports = exports.default;
module.exports.default = exports.default;